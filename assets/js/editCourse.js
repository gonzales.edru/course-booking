let name = document.querySelector("#courseName")
let price = document.querySelector("#coursePrice")
let description = document.querySelector("#courseDescription")
let instructor = document.querySelector("#courseInstructor")
let params = new URLSearchParams(window.location.search)
let courseId
courseId = params.get('courseId')

fetch(`https://course-booking-final.herokuapp.com/api/courses/${courseId}`).then(res => {
    return res.json()
})
.then(data => {
	// assign the current values as placeholders
	courseName.placeholder = data.name
	courseName.value = data.name
	coursePrice.placeholder = data.price
	coursePrice.value = data.price
	courseDescription.placeholder = data.description
	courseDescription.value = data.description
	courseInstructor.placeholder = data.instructor
	courseInstructor.value = data.instructor
	document.querySelector("#editCourse").addEventListener("submit", (e) => {
		e.preventDefault()

		let courseName = name.value
		let courseDescription = description.value
		let coursePrice = price.value
		let courseInstructor = instructor.value

		let token = localStorage.getItem('token')

		fetch('https://course-booking-final.herokuapp.com/api/courses/update', {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                courseId: courseId,
                name: courseName,
                description: courseDescription,
                price: coursePrice,
                instructor: courseInstructor
            })
        })
        .then(res => {
            return res.json()
        })
        .then(data => {
            //creation of new course successful
            if(data === true){
                //redirect to courses index page
                window.location.replace("./courses.html")
            }else{
                //error in creating course, redirect to error page
                alert("Something went wrong")
            }
        })
	})
})

