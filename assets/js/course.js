//instantiate a URLSearchParams object so we can execute methods to access parts of the query string

let params = new URLSearchParams(window.location.search)
let courseId = params.get('courseId')
let token = localStorage.getItem("token")
let userId = localStorage.getItem("id")
let courseName = document.querySelector("#courseName")
let courseDesc = document.querySelector("#courseDesc")
let coursePrice = document.querySelector("#coursePrice")
let enrollContainer = document.querySelector("#enrollContainer")

fetch(`https://course-booking-final.herokuapp.com/api/courses/${courseId}`)
.then((res) => res.json())
.then((data) => {
	courseName.innerHTML = data.name
	courseDesc.innerHTML = data.description
	coursePrice.innerHTML = data.price
	enrollContainer.innerHTML = `<button id='enrollButton' class = "btn btn-block btn-primary">Enroll</button>`
	let enrollButton = document.querySelector("#enrollButton")
	enrollButton.addEventListener("click", () => {
		//enroll the user for the course
		
		fetch('https://course-booking-final.herokuapp.com/api/users/enroll', {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				courseId: courseId,
				userId: userId
			})
		})
		.then(res => {
			return res.json()

		})
		.then(data => {	
			if(data.auth == "failed")
			{
				window.location.replace("./login.html")
			}
			else
			{
				alert("Enrollment Successful")
				window.location.replace("./courses.html")
			}
		})
	})
})