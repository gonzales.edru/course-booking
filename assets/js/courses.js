let adminUser = localStorage.getItem("isAdmin")
let modalButton = document.querySelector("#adminButton")
let cardFooter
let userId = localStorage.getItem("id")

if(adminUser == "false" || !adminUser)
{
	modalButton.innerHTML = null
	fetch('https://course-booking-final.herokuapp.com/api/courses', {
			method: "POST",	
			headers: {
				"Content-Type": "application/json",
			},
			body: JSON.stringify({
				userId: userId
			})
	}).then(res => res.json()).then(data => { 
		let courseData 
		console.log(data)
		if(data.length < 1)
		{ 
			courseData = "No courses available" 
		}
		else
		{ 
			courseData = data.map(course => 
			{
					cardFooter = `<a href="./course.html?courseId=${course._id}"
					value = ${course._id} class="btn btn-primary text-white btn-block
					editButton">Select Course</a>`
					return (`
						<div class="col-md-6 col-lg-6 my-3">
							<div class="card text-justify">
								<div class="card-body">
									<h5 class="card-title">${course.name}</h5>
									<p class="card-text text-left">${course.description}</p>
									<p class="card-text text-left  mt-4">Instructor: ${course.instructor}</p>
									<p class="card-text text-right">₱ ${course.price}</p>
								</div>
								<div class = "card-footer">
									${cardFooter}
								</div>
							</div>
						</div>
					`) 

				

			//sice the collection is an array use the join method to indicate the seperator of each element
			}).join("") 
		}

		let container = document.querySelector('#coursesContainer')
		container.innerHTML = courseData
	})
}
else
{
	modalButton.innerHTML = `<div class="pl-5">
	<a href="./addCourse.html" class="btn btn-block btn-primary text-white ">Add Course</a>
	</div>`

	fetch('https://course-booking-final.herokuapp.com/api/courses/admin').then(res => res.json()).then(data => { 
		
		let courseData 

		if(data.length < 1)
		{ 
			courseData = "No courses available" 
		}
		else
		{ 
			courseData = data.map(course => 
			{
				if(course.isActive === true)
				{
					cardFooter = 
							`
								<a href="./editCourse.html?courseId=${course._id}"
								value = {course._id} class="btn btn-primary text-white btn-block
								editButton">Edit Course</a>

								<a href="./deleteCourse.html?courseId=${course._id}"
								value = ${course._id} class="btn btn-primary text-white btn-block
								editButton">Delete Course</a>

							`
				}
				else
				{
					cardFooter = 
							`
								<a href="./editCourse.html?courseId=${course._id}"
								value = {course._id} class="btn btn-primary text-white btn-block
								editButton">Edit Course</a>

								<a href="./activateCourse.html?courseId=${course._id}"
								value = ${course._id} class="btn btn-primary text-white btn-block
								editButton">Add Course</a>

							`
				}
				
				return (`
					<div class="col-md-6 col-lg-6 my-3">
						<div class="card ">
							<div class="card-body">
								<h5 class="card-title"><a href="./courseDetails.html?courseId=${course._id}&courseName=${course.name}">${course.name}</a></h5>
								<p class="card-text text-left">${course.description}</p>
								<p class="card-text text-left mt-4">Instructor: ${course.instructor}</p>
								<p class="card-text text-right">₱ ${course.price}</p>
								
							</div>
							<div class = "card-footer">
								${cardFooter}
							</div>
						</div>
					</div>
				`) 
			//sice the collection is an array use the join method to indicate the seperator of each element
			}).join("") 
		}

		let container = document.querySelector('#coursesContainer')
		container.innerHTML = courseData
		
	})
}

//fetch the courses from our API
