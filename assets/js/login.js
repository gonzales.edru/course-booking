let loginForm = document.querySelector("#logInUser")
let home = document.querySelector("#homeTest")
loginForm.addEventListener("submit", (e) => {
	e.preventDefault()

	let email = document.querySelector("#userEmail").value
	let password = document.querySelector("#password").value

	if(email === "" || password === ""){
		alert("Please input your email or password")
	}
	else{
		fetch('https://course-booking-final.herokuapp.com/api/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': "application/Json"
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			if(data === false)
			{
				alert("Incorrect Username or Password")
			}
			if(data.access){
				localStorage.setItem('token', data.access)
				fetch('https://course-booking-final.herokuapp.com/api/users/details', {
					headers: {
					Authorization: `Bearer ${data.access}`
					}
				})
				.then(res => {
					return res.json()
				})
				.then(data => {
					localStorage.setItem("id", data._id)
					localStorage.setItem("isAdmin", data.isAdmin)
					window.location.replace("./courses.html")

				})
			}
		})
	}

}) 
