let params = new URLSearchParams(window.location.search)
let courseId
courseId = params.get('courseId')

fetch("https://course-booking-final.herokuapp.com/api/courses/delete", {
    method: "PUT",
    headers: 
    {
        "Content-Type": "application/json"
    },
    body: JSON.stringify
    ({
        courseId: courseId
    })
})
.then(res => {
    return res.json()
})
.then(data => {
    if(data === true){
        window.location.replace('./courses.html')
    }
    else{
        alert("Something went wrong")
    }
})