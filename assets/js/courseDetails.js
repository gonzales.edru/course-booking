let adminVerify = localStorage.getItem("isAdmin")
let userProfile = document.querySelector("#userProfile")
let token = localStorage.getItem("token")
let userId = localStorage.getItem("id")
let params = new URLSearchParams(window.location.search)
let courseId = params.get("courseId")
let courseName = params.get("courseName")
console.log(adminVerify)
console.log(courseId)
console.log(courseName)
//fetch the courses from our API
fetch(`https://course-booking-final.herokuapp.com/api/courses/details`, {
		method: "POST",
		headers: {
			"Content-Type": "application/json",
		},
		body: JSON.stringify({
			courseId: courseId
		})
	})
	.then(res => res.json()).then(data => { 
		let courseData
		console.log(data)
		if (userId === null) {
			window.location.replace("./login.html")
		}
		else
		{		
			courseDataBody = data.resultsFromFindCourse.map(details => {
				console.log(details)
				return(`
						<div>
							<div class="mb-0 d-flex">
							<p class="col-md-3" id="courseName">${details.firstName +" "+ details.lastName}</p><p class="col-md-3 d-flex"> ${data.resultsFromFind.instructor}</p><p class="col-md-3 d-flex">Count :</p>
							<p class="col-md-3"><a>Remove</a></p>
						</div>
					`)	
				
			})
			courseData = 	`
							<div class="col-md-12 container-fluid p-0">
								<div id="profileContainer">
									<div class="col-md-12 p-0" >
										<div class="card">
											<div class="card-header pl-0">
												<h6>${courseName}</h6>
											</div>
											<div class="card-body" id="profileCourseEnrolled">
												<div class="mb-0 d-flex ">
													<p class="col-md-3">Enrolled Students:</p><p class="col-md-3 d-flex"> Instructor </p>
													<p class="col-md-3 d-flex"> Enrolled Count </p><p class="col-md-3 d-flex"> Action </p>
													</div>
												</div>	
												${courseDataBody}
											</div>
										</div>
									</div>
								</div>	
							</div>	
						`
		}
		let container = document.querySelector('#adminContainer')
		container.innerHTML = courseData

	})

