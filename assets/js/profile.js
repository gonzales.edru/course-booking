let adminVerify = localStorage.getItem("isAdmin")
let userProfile = document.querySelector("#userProfile")
let token = localStorage.getItem("token")
let userId = localStorage.getItem("id")
console.log(adminVerify)
//fetch the courses from our API
fetch('https://course-booking-final.herokuapp.com/api/users/profile', {
		method: "POST",
		headers: {
			"Content-Type": "application/json",
			'Authorization': `Bearer ${token}`
		},
		body: JSON.stringify({
			userId: userId
		})
	})
	.then(res => res.json()).then(data => { 
		let userData
		let adminProfileData
		let adminData
		console.log(data)
		if (userId === null) {
			window.location.replace("./login.html")
		}
		else
		{
			if(data.length < 1){
				userData = "No profile found"
			}
			else
			{
				if(adminVerify == "false" || !adminVerify)
				{
					courseEnrolled = data.resultsFromFindCourse.map(enrollment => {
						return(
								`
									<div>
										<div class="mb-0 d-flex">
									    	<p class="col-md-3 d-flex" id="courseName">${enrollment.name}</p><p class="col-md-3" id="courseInstructor"> Instructor</p><p class="col-md-3" id="courseEnrolledCount">${enrollment.enrollees.length}</p>
									    	<p class="col-md-3 courseAction btn" courseId=${enrollment._id}>Remove</p>
										</div>
									</div>
								`
							)
					})
					userData =

									`
										<div class="col-md-12 p-0">
											<div class="card">
												<div class="card-header">
													 PROFILE
												</div>
												<div class="card-body">
													<div class="mb-0 d-flex">
														<p class="col-md-6 d-flex">Email :</p><p class="col-md-6">${data.resultsFromFind.email}</p>
													</div>
													<div class="mb-0 d-flex">
														<p class="col-md-6 d-flex">Name :</p><p class="col-md-6">${data.resultsFromFind.firstName} ${""} ${data.resultsFromFind.lastName}</p>
													</div>
													<div class="mb-0 d-flex">
														<p class="col-md-6 d-flex">Contact Info :</p><p class="col-md-6">${data.resultsFromFind.mobileNo}</p>
													</div>
												</div>
											</div>
											<div id="profileContainer">
												<div class="col-md-12 p-0">
													<div class="card">
														<div class="card-header">
															COURSES ENROLLED
														</div>
														<div class="card-body" id="profileCourseEnrolled">
															<div class="mb-0 d-flex ">
															  	<p class="col-md-3 d-flex">Course Name :</p><p class="col-md-3"> Instructor </p>
															  	<p class="col-md-3"> Enrolled Count </p><p class="col-md-3"> Action </p>
															</div>
														</div>	
														${courseEnrolled}
													</div>
												</div>
											</div>
										</div>

									`
				}
				else
				{

					fetch("https://course-booking-final.herokuapp.com/api/users/").then(res => res.json()).then(data => {
						 	adminProfileData = data.map(users => {
						 	return(`
											<tr>
												<td>${users.firstName + " " + users.lastName}</td>
												<td>${users.email}</td>
												<td>${users.mobileNo}</td>
												<td  class="pl-5"><a href="../pages/adminProfile.html?userId=${users._id}" style="color:black">View</a></td>
											</tr>		
												
							`

						 	)
							 
							}).join("")	
							adminData =
							`		
							<table class="table">
							  <thead class="thead-dark">
								    <tr>
								      <th scope="col">Name</th>
								      <th scope="col">Email</th>
								      <th scope="col">Mobile No</th>
								      <th scope="col">Course Enrolled</th>
								    </tr>
							  </thead>
							  <tbody>
							   		${adminProfileData}
							  </tbody>
							</table>
								`
							let adminContainer = document.querySelector("#adminProfileContainer")
							adminContainer.innerHTML = adminData			
					})
					
				}
			}
			
		}
		let container = document.querySelector('#profileContainer')
		container.innerHTML = userData
		let courseAction = document.querySelectorAll(".courseAction")
		courseAction.forEach(courses => {
			courses.addEventListener("click", (e) => {
				courseId = e.target.getAttribute("courseid")
				fetch('https://course-booking-final.herokuapp.com/api/users/removeCourse', {
						method: "DELETE",
						headers: {
							"Content-Type": "application/json"
						},
						body: JSON.stringify({
							userId: userId,
							courseId: courseId
						})
					})
					.then(res => res.json())
					.then(data => {
						window.location.replace(`./adminProfile.html?userId=${userId}`)
					})
		})

	})

})