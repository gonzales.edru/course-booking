let formSubmit = document.querySelector("#createCourse")

formSubmit.addEventListener("submit", (e) => {
	e.preventDefault()

	let courseName = document.querySelector("#courseName").value

	let description = document.querySelector("#courseDescription").value

	let price = document.querySelector("#coursePrice").value

	let instructor = document.querySelector("#courseInstructor").value

	let token = localStorage.getItem('token')
	if (courseName !== null && description !== null && price !== null && instructor !== null) 
	{
		fetch("https://course-booking-final.herokuapp.com/api/courses/add", {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				name: courseName,
				description: description,
				price: price,
				instructor: instructor
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			if(data === true){
				window.location.replace('./courses.html')
			}
			else{
				alert("Something went wrong")
			}
		})
	}
})